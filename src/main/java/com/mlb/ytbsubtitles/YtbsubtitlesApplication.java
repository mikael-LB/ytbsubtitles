package com.mlb.ytbsubtitles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YtbsubtitlesApplication {

	public static void main(String[] args) {
		SpringApplication.run(YtbsubtitlesApplication.class, args);
	}
	
}
