package com.mlb.ytbsubtitles.bll;

import java.util.List;

import com.mlb.ytbsubtitles.bo.File;

public interface FileManager {
	public int addFile(File file);
	public File getFile(String id);
	public List<File> getAllFile();
	public String deleteSubtitleFromFile(String id, String index);
	public File findFileForAnIndex(String index) throws StorageFileNotFoundException, FileNumberFormatException;
	public void deleteFileById(String id) throws StorageFileNotFoundException, FileNumberFormatException;
	public void renameFile(String id, String newName);
	public void copyFile(String id);
}
