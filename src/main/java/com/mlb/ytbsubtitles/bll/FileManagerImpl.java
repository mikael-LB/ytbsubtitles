package com.mlb.ytbsubtitles.bll;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlb.ytbsubtitles.bo.File;
import com.mlb.ytbsubtitles.bo.Subtitle;
import com.mlb.ytbsubtitles.dal.FileRepository;

@Service
public class FileManagerImpl implements FileManager {

	@Autowired
	FileRepository daoFile;

	/**
	 * Method to save or update a File in database
	 * return nothing
	 */
	@Override
	@Transactional
	public int addFile(File file) {
		daoFile.save(file);
		return file.getId();
	}

	/**
	 * Method to get a File for a given id
	 * return the file if found or throw an exception
	 */
	@Override
	public File getFile(String id) {

		File file;
		try {
			file = daoFile.findById(Integer.parseInt(id)).get();
		} catch (NumberFormatException e) {
			throw new NotIntException ("Le paramètre doit être un nombre entier");
		}
		return file;
	}

	/**
	 * Method to get all File in database
	 * return a List of all files
	 */
	@Override
	public List<File> getAllFile() {
		return (List<File>) daoFile.findAll();
	}

	/**
	 * Method to delete a Subtitle from a File
	 * return an empty string if ok, or return an error message
	 */
	@Override
	@Transactional
	public String deleteSubtitleFromFile(String id, String index) {
		int idInInt;
		int indexInInt;
		File file;

		// try to find the file by the id
		try {
			idInInt = Integer.parseInt(id);
			daoFile.existsById(idInInt);
			//no catch so
			file = daoFile.findById(idInInt).get();
		} catch (NumberFormatException e) {
			throw new NotIntException("Le paramètre doit être un nombre entier");
		} catch (Exception e) {
			return "Le fichier demandé n'existe pas en base de données";
		}


		// try to find the subtitle by index in LIST, not in database
		try {
			// get the list of Subtitles for the file
			// then get the subtitle by the index-1 in this list (index start at 0)
			// then get the id of the selected subtitle
			indexInInt = Integer.parseInt(index);
			file.getLstSubtitle().get(indexInInt-1).getId();

			// no catch
			// Subtitle is found in the list so we can delete it
			// passing the index to remove from the list
			file.removeSubtitle(indexInInt-1);
			// then save the file to update database
			daoFile.save(file);
			// no error
			return "";

		} catch (Exception e) {
			// thrown when user give a string for index or when index does exist
			// do nothing
			return "Cet élément n'existe pas";
		}
	}

	/**
	 * This method get all file from database file table,
	 * then try to find the file by the index in the list
	 * if ok return file, if no file throw exception
	 */
	@Override
	public File findFileForAnIndex(String index)
			throws StorageFileNotFoundException, FileNumberFormatException {

		List<File> lstFiles = this.getAllFile();
		File file;
		int indexToFind;

		// try to cast index to int
		try {
			// if index is String thing, a FileNumberFormatException is thrown
			indexToFind = Integer.parseInt(index);
			file = lstFiles.get(indexToFind -1);
			return file;
		} catch (NumberFormatException e) {
			throw new FileNumberFormatException("Saisir un nombre");
		} catch (Exception e) {
			throw new StorageFileNotFoundException("Fichier non trouvé");
		}
	}

	/**
	 * This method check if the file exist in the database
	 * if user pass a string which can't be parse to int
	 * an exception is thrown
	 */
	@Override
	@Transactional
	public void deleteFileById(String id)
			throws StorageFileNotFoundException, FileNumberFormatException {
		// try to find the file by the id
		try {
			int idInInt = Integer.parseInt(id);
			daoFile.existsById(idInInt);
			//no catch so
			daoFile.deleteById(idInInt);
		} catch (NumberFormatException e) {
			throw new NotIntException("Le paramètre doit être un nombre entier");
		} catch (Exception e) {
			throw new StorageFileNotFoundException(
					"Le fichier demandé n'existe pas en base de données");
		}
	}

	/**
	 * Method to rename a File
	 */
	@Override
	public void renameFile(String id, String newName)
			throws StorageFileNotFoundException, FileNumberFormatException {
		// try to find the file by the id
		try {
			File file = this.getFile(id);
			//
			newName = newName.trim();
			//
			newName = newName.replaceAll("[ !@#$%^&*()+=\\[\\]{};':\"\\\\|,.<>\\/?]*", "");
			// limite entre 2 et 30 caractères
			
			file.setFilename(newName);
			this.addFile(file);
		} catch (NumberFormatException e) {
			throw new NotIntException("Le paramètre doit être un nombre entier");
		} catch (Exception e) {
			throw new StorageFileNotFoundException(
					"Entre 2 et 30 caractères, - et _ autorisés");
		}

	}

	/**
	 * Method to copy a File
	 * no return
	 */
	@Override
	@Transactional
	public void copyFile(String id) {
		
		File newFile;
		List<File> lstFiles = daoFile.findAll();
		
		try {
			// retrieve last index of all files
			File lastFile = lstFiles.get(lstFiles.size()-1);
			int lastIdOfList = lastFile.getId();
			
			File actualFile = getFile(id);
			
			// new File with same filename
			newFile = new File(actualFile.getFilename());
			// then copy subtitles
			List<Subtitle> lstSubtitles = actualFile.getLstSubtitle();
			for (Subtitle subtitle : lstSubtitles) {
				// duplicate the subtitle
				Subtitle newSubtitle = new Subtitle(
						subtitle.getStartTime(),
						subtitle.getEndTime(),
						subtitle.getText(),
						subtitle.getTextSecondLine(),
						newFile);
				// then add the newSubtitle to theNewFile
				newFile.addSubtitle(newSubtitle);
			}
			// then save
			daoFile.save(newFile);
		} catch (NumberFormatException e) {
			throw new NotIntException("Le paramètre doit être un nombre entier");
		} catch (Exception e) {
			throw new StorageFileNotFoundException(
					"Une erreur est survenue");
		}
	}

}
