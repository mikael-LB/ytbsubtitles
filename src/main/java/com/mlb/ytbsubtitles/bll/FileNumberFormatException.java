package com.mlb.ytbsubtitles.bll;

public class FileNumberFormatException extends NumberFormatException{

	public FileNumberFormatException (String message) {
		super(message);
	}
}
