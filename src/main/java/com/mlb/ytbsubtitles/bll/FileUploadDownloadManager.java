package com.mlb.ytbsubtitles.bll;

import java.io.InputStream;

import org.springframework.core.io.InputStreamResource;
import org.springframework.web.multipart.MultipartFile;

public interface FileUploadDownloadManager {
	
	public void saveSRTFile(MultipartFile file);
	public InputStream loadSRTFile(int id);
}
