package com.mlb.ytbsubtitles.bll;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.mlb.ytbsubtitles.bo.File;
import com.mlb.ytbsubtitles.bo.Subtitle;
import com.mlb.ytbsubtitles.dal.FileRepository;

@Service
public class FileUploadDownloadManagerImpl implements FileUploadDownloadManager {

	@Autowired
	FileRepository daoFile;

	@Override
	@Transactional
	public void saveSRTFile(MultipartFile file) {

		File fileToCreate = new File();
		List<Subtitle> lstSubtitles = new ArrayList<Subtitle>();
		String filename = StringUtils.cleanPath(file.getOriginalFilename());
		Scanner s;

		try {
			// extension must be .srt
			if (!filename.endsWith(".srt")) {
				throw new StorageException("Wrong extension for file"+ filename);
			}
			// remove blank spaces begin end of filename
			filename.trim();
			// remove extension .srt to store in database
			filename = filename.substring(0, (filename.length()-4));
			// file not empty
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file " + filename);
			}
			//			Don't store the file, only datas, don't check here
			//			if (filename.contains("..")) {
			//				// This is a security check
			//				throw new StorageException(
			//						"Cannot store file with relative path outside current directory "
			//								+ filename);
			//			}

			// create the new file in database
			fileToCreate.setFilename(filename);
			File fileCreated = daoFile.save(fileToCreate);

			try (InputStream inputStream = file.getInputStream()) {
				//			Files.copy(inputStream, this.rootLocation.resolve(filename),
				//					StandardCopyOption.REPLACE_EXISTING);
				s = new Scanner(inputStream);
				try {
					while (s.hasNextLine()) {

						// id is auto so pass to next line
						// int id= Integer.parseInt(s.nextLine());
						s.nextLine();

						String time = s.nextLine();
						String startTime = time.substring(0, 12);
						String endTime = time.substring(time.length()-12, time.length());
						String text = s.nextLine();
						// possibly have no blank line in file after the last subtitle
						String textSecondLine = "";
						if (s.hasNextLine()) {
							textSecondLine = s.nextLine();
							if (textSecondLine.isBlank()){
								textSecondLine = null;
							} else {
								// last line of a subtitle block is blank
								s.nextLine();
							}
						} else {
							// for secondtext of last line before end of file
							textSecondLine = null;
						}

						Subtitle subtitle = new Subtitle(
								startTime,
								endTime,
								text,
								textSecondLine,
								fileCreated
								);

						lstSubtitles.add(subtitle);
						System.out.println(subtitle.toString());
					}
					// all lines have been read, save to update the file
					fileCreated.setLstSubtitle(lstSubtitles);
					daoFile.save(fileCreated);
				} finally {
					s.close();
				}
			}
		}
		catch (IOException e) {
			throw new StorageException("Failed to store file " + filename, e);
		}
	}

	@Override
	public ByteArrayInputStream loadSRTFile(int id) {

		// getting the file from database
		File fileToDownload = daoFile.getOne(id);

		List<Subtitle>lstSubtitles = new ArrayList<Subtitle>();
		lstSubtitles = fileToDownload.getLstSubtitle();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		Integer cpt = 1;

		for (Subtitle subtitle : lstSubtitles) {
			try {
				if (cpt != 1) {
					// one line blank between each record
					baos.write("\n".getBytes());
				}
				// not the id cause it can be any int value
				//baos.write(subtitle.getId().toString().getBytes());
				// but add cpt which begin at 1
				baos.write(cpt.toString().getBytes());
				baos.write("\n".getBytes());
				baos.write(subtitle.getStartTime().getBytes());
				baos.write(" --> ".getBytes());
				baos.write(subtitle.getEndTime().getBytes());
				baos.write("\n".getBytes());
				baos.write(subtitle.getText().getBytes());
				if (subtitle.getTextSecondLine() != null
						&& !subtitle.getTextSecondLine().isEmpty()) {
					baos.write("\n".getBytes());
					baos.write(subtitle.getTextSecondLine().getBytes());
					// backslash n see above in if cpt was not 1
					// cause no line after last record
				}
				// go next line
				baos.write("\n".getBytes());
				
				// increment cpt
				cpt++;
			} catch (IOException e) {
				throw new StorageException("Erreur de lecture", e);
			}
		}

		byte[] subInByte = baos.toByteArray();

		ByteArrayInputStream bais = new ByteArrayInputStream(subInByte);

		return bais;
	}

}
