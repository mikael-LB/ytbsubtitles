package com.mlb.ytbsubtitles.bll;

public class NotIntException extends NumberFormatException {
	
	public NotIntException (String message) {
		super(message);
	}
}
