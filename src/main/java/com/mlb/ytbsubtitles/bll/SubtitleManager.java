package com.mlb.ytbsubtitles.bll;

import com.mlb.ytbsubtitles.bo.Subtitle;

public interface SubtitleManager {
	public void addSubtitle(Subtitle subtitle);
	public Subtitle getSubtitle(String id);
	public Subtitle getSubtitleByIndexForFile(String id, String index);
}
