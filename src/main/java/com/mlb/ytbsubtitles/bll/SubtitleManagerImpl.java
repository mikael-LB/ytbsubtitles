package com.mlb.ytbsubtitles.bll;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mlb.ytbsubtitles.bo.Subtitle;
import com.mlb.ytbsubtitles.dal.SubtitleRepository;

@Service
public class SubtitleManagerImpl implements SubtitleManager {

	@Autowired
	SubtitleRepository daoSubtitle;
	
	/**
	 * Mehod to save or update a Subtitle
	 * return nothing
	 */
	@Override
	@Transactional
	public void addSubtitle(Subtitle subtitle) {
		try {
			daoSubtitle.save(subtitle);
		} catch (Exception e) {
			throw new StorageException("erreur");
		}
	}

	/**
	 * Method to get a Subtitle by an id
	 * return a Subtitle
	 */
	@Override
	public Subtitle getSubtitle(String id) {
		return daoSubtitle.findById(Integer.parseInt(id)).get();
	}

	/**
	 * This method use the bidirectional association
	 * to check if user has given an id for an existing File in database
	 */
	@Override
	public Subtitle getSubtitleByIndexForFile(String id, String index)
			throws StorageFileNotFoundException, FileNumberFormatException {
		
		int idFileToFind;
		int idSubtitleToFind;
		List<Subtitle> lstSubtitle = new ArrayList<Subtitle>();
		
		try {
			idFileToFind = Integer.parseInt(id);
			idSubtitleToFind = Integer.parseInt(index);
			
			// First retrieve all subtitle for a file
			lstSubtitle = daoSubtitle.findAllForGivenFileId(idFileToFind);
			//then retrieve and return the subtitle in this list
			return lstSubtitle.get(idSubtitleToFind -1);
			
		} catch (NumberFormatException e) {
			throw new NotIntException ("Le paramètre doit être un nombre entier");
		} catch (Exception e) {
			throw new StorageFileNotFoundException(
					"L'index n'existe pas en base de données");
		}
		
	}

}
