package com.mlb.ytbsubtitles.bo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
public class File {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@NotNull
	@Size(min=2, max=254)
	private String filename;
	
	@OneToMany(mappedBy="file", cascade = {CascadeType.ALL}, orphanRemoval = true)
	private List<Subtitle> lstSubtitle = new ArrayList<Subtitle>();

	public File() {
		super();
	}

	public File(String filename) {
		super();
		this.filename = filename;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public List<Subtitle> getLstSubtitle() {
		return this.lstSubtitle;
	}

	public void setLstSubtitle(List<Subtitle> lstSubtitle) {
		this.lstSubtitle = lstSubtitle;
	}
	
	/**
	 * Method which allow to add a subtitle to the list
	 * of subtitle of a file
	 * @param subtitle
	 */
	public void addSubtitle(Subtitle subtitle) {
		// for bidirectional we have to set the File in subtitle
		subtitle.setFile(this);
		this.lstSubtitle.add(subtitle);
	}
	
	/**
	 * Method to delete a subtitle by his id
	 * @param id
	 */
	public void removeSubtitle(int id) {
		this.lstSubtitle.remove(id);
	}

	@Override
	public String toString() {
		return "File [Id=" + id + ", filename=" + filename + ", lstSubtitle=" + lstSubtitle + "]";
	}
}
