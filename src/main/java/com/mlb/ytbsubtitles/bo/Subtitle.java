package com.mlb.ytbsubtitles.bo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Subtitle {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	@Size(min=12, max=12)
	private String startTime;
	@Size(min=12, max=12)
	private String endTime;
	@Size(max=254)
	private String text;
	@Size(max=254)
	private String textSecondLine;
	
	// bidirectional
	@ManyToOne
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	File file;
	
	public Subtitle() {
		super();
	}
	
	public Subtitle(String startTime, String endTime, 
			String text, String textSecondLine, File file) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.text = text;
		this.textSecondLine = textSecondLine;
		this.file = file;
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getStartTime() {
		return this.startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	
	public String getEndTime() {
		return this.endTime;
	}
	
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getText() {
		return this.text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getTextSecondLine() {
		return textSecondLine;
	}

	public void setTextSecondLine(String textSecondLine) {
		this.textSecondLine = textSecondLine;
	}
	
	public File getFile() {
		return this.file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	@Override
	public String toString() {
		return "Subtitle [id=" + id 
				+ ", startTime=" + startTime 
				+ ", endTime=" + endTime 
				+ ", Text=" + text 
				+ " TextSecondLine=" + textSecondLine 
				+ ", File=" + file.getFilename() + "]";
	}
	
}
