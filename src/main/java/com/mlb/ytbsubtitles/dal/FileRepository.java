package com.mlb.ytbsubtitles.dal;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mlb.ytbsubtitles.bo.File;

public interface FileRepository extends JpaRepository<File, Integer> {
}
