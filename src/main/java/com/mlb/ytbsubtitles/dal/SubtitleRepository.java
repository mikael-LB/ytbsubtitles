package com.mlb.ytbsubtitles.dal;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.mlb.ytbsubtitles.bo.Subtitle;


public interface SubtitleRepository extends CrudRepository<Subtitle, Integer> {

	@Query (value="SELECT * FROM subtitle WHERE file_id = ?1", nativeQuery = true)
	List<Subtitle> findAllForGivenFileId(int idFileToFind);
	
}
