package com.mlb.ytbsubtitles.ihm;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mlb.ytbsubtitles.bll.FileManager;
import com.mlb.ytbsubtitles.bll.NotIntException;
import com.mlb.ytbsubtitles.bll.StorageFileNotFoundException;
import com.mlb.ytbsubtitles.bo.File;

@Controller
@SessionScope
public class FileController {

	@Autowired
	private FileManager fileManager;


	/**
	 * Call when getting the url /Accueil
	 * @return
	 */
	@GetMapping("/Accueil")
	public String getAccueilPage(Model model){
		List<File> lstFile = fileManager.getAllFile();
		model.addAttribute("lstFile", lstFile);
		return "welcome";
	}

	/**
	 * Call when user select menu-item "new file"
	 * @param model
	 * @return page newFile.html
	 */
	@GetMapping("/NewFile")
	public String getNewFilePage(Model model) {
		File file = new File("");
		model.addAttribute("file",file);

		return "newFile";
	}

	/**
	 * Call when user validate the file form
	 * @param file
	 * @param bindingResult
	 * @param model
	 * @return same page with errors or go to welcome.html
	 */
	@PostMapping("/NewEmptyFile")
	public String newEmptyFile(@Valid File file, 
			BindingResult bindingResult, 
			Model model,
			RedirectAttributes redirectAttributes) {

		if (bindingResult.hasErrors()) {
			return "newFile";
		}

		// Create the file in database
		int id = fileManager.addFile(file);

		//passage du id et message de succès
		redirectAttributes.addAttribute("id", id).addFlashAttribute(
				"okMessage", "Fichier créé avec succès");

		return "redirect:FileSelected/{id}";
	}

	/**
	 * Method called when user select a file in list
	 * or user has created a new file
	 * Note : model can receive a parameter named "okMessage"
	 * from the newEmptyFile method.
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping(value="/FileSelected/{id}")
	public String fileSelected(@PathVariable String id, Model model) {

		File file = fileManager.getFile(id);
		// Remember : possible okMessage attribute set by redirect
		model.addAttribute("file",file);

		return "fileSelected";
	}

	/**
	 * Method call when user clic on delete file
	 * @param id
	 * @param model
	 * @return welcome.html
	 */
	@GetMapping(value="/DeleteFile/{id}")
	public String deleteFile(@PathVariable String id, Model model) {

		fileManager.deleteFileById(id);

		// get the list modified in database
		List<File> lstFile = fileManager.getAllFile();
		model.addAttribute("lstFile", lstFile);

		return "welcome";
	}

	/**
	 * Method call when user clic to rename file
	 * @param id
	 * @param newName
	 * @param model
	 * @return
	 */
	@PostMapping(value="/RenameFile/{id}")
	public String renameFile(@PathVariable String id,
			@RequestParam("newname") String newName,
			Model model) {
		try {
			fileManager.renameFile(id, newName);
		} catch (StorageFileNotFoundException e) {
			model.addAttribute("errorRename", e.getMessage());
			model.addAttribute("file", fileManager.getFile(id));
			return "fileSelected";
		}
		model.addAttribute("file", fileManager.getFile(id));
		return "fileSelected";
	}

	/**
	 * Method call when user clic to duplicate file
	 * @param id is the id of the file in database
	 * @param model
	 * @return welcome.html with list of files
	 */
	@GetMapping(value="/CopyFile/{id}")
	public String copyFile(@PathVariable String id, Model model) {

		fileManager.copyFile(id);

		// get the list modified in database
		List<File> lstFile = fileManager.getAllFile();
		model.addAttribute("lstFile", lstFile);

		return "welcome";
	}


	/**
	 * Method intercept NotIntException
	 * and return a JSON responseEntity with errorMessage
	 * @param exc
	 * @return
	 */
	@ExceptionHandler(NotIntException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<?> handleNotIntException(NotIntException exc) {

		return ResponseEntity.badRequest()
				.contentType(MediaType.APPLICATION_JSON)
				.body("{\"errorMessage\":\"Mauvaise requête\"}");
	}

	/**
	 * Method intercept HttpRequestMethodNotSupportedException
	 * and return a JSON responseEntity with errorMessage
	 * @param exc
	 * @return
	 */
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<?> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException exc) {

		return ResponseEntity.badRequest()
				.contentType(MediaType.APPLICATION_JSON)
				.body("{\"errorMessage\":\"Mauvaise requête\"}");
	}
}
