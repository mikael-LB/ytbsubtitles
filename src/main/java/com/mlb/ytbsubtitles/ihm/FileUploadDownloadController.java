package com.mlb.ytbsubtitles.ihm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.multipart.MultipartFile;

import com.mlb.ytbsubtitles.bll.FileManager;
import com.mlb.ytbsubtitles.bll.FileNumberFormatException;
import com.mlb.ytbsubtitles.bll.FileUploadDownloadManager;
import com.mlb.ytbsubtitles.bll.StorageFileNotFoundException;

@Controller
@SessionScope
public class FileUploadDownloadController {

	@Autowired
	private FileManager fileManager;
	@Autowired
	private FileUploadDownloadManager fileUploadDownloadManager;

	/**
	 * Call when user want to UPLOAD file
	 * @param model
	 * @return uploadPage
	 */
	@GetMapping("/wantToUploadFile")
	public String WantToUploadFile(Model model) {

		return "upload-download/uploadForm";
	}

	/**
	 * Call when user post the UPLOAD form
	 * @param file
	 * @param model
	 * @return
	 */
	@PostMapping("/upload")
	public String FileUpload(@RequestParam("file") MultipartFile file, Model model) {

		String okMessage = "ok";

		fileUploadDownloadManager.saveSRTFile(file);

		model.addAttribute("okMessage", okMessage);
		return "upload-download/uploadForm";

	}

	/**
	 * Call when user want to DOWNLOAD file
	 * @param model
	 * @return uploadPage
	 */
	@GetMapping("/wantToDownloadFile")
	public String WantToDownloadFile(Model model) {

		return "upload-download/downloadForm";
	}

	/**
	 * Call when user post the form with id to DOWNLOAD
	 * @param id
	 * @param mode
	 * @return
	 */
	@PostMapping("/download")
	public ResponseEntity<Resource> getFile(
			@RequestParam("index") String index, Model model)
					throws StorageFileNotFoundException {

		// can't do try catch and return a ResponseEntity with a String type
		// so Exception are catch and Response created directly with
		// the method handleStorageFileNotFound() see below

		// try to find by index in list of file in database
		fileManager.findFileForAnIndex(index);

		// no error at this point so the file exist,
		// define filename and get id in database
		String filename = fileManager.findFileForAnIndex(index).getFilename() + ".srt";
		
		int idFileToDownload = fileManager.findFileForAnIndex(index).getId();
		// then create stream
		InputStreamResource fileStream = new InputStreamResource(fileUploadDownloadManager.loadSRTFile(idFileToDownload));
		// then return file as stream

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filename + "\"")
				.contentType(MediaType.parseMediaType("text/srt"))
				.body(fileStream);


	}

	/**
	 * Method intercept StorageFileNotFoundException
	 * and return the download page with errorMessage
	 * @param exc
	 * @param model
	 * @return
	 */
	@ExceptionHandler(StorageFileNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String handleStorageFileNotFound(StorageFileNotFoundException exc, Model model) {
		
		model.addAttribute("errorMessage", exc.getMessage());
		return "upload-download/downloadForm";
	}
	
	/**
	 * Method intercept NumberFormatException
	 * and return the download page with errorMessage
	 * @param exc
	 * @param model
	 * @return
	 */
	@ExceptionHandler(FileNumberFormatException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public String handleFileNumberFormat(FileNumberFormatException exc, Model model) {
		
		model.addAttribute("errorMessage", exc.getMessage());
		return "upload-download/downloadForm";
	}
}
