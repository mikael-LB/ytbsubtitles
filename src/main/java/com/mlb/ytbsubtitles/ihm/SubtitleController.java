package com.mlb.ytbsubtitles.ihm;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.annotation.SessionScope;

import com.mlb.ytbsubtitles.bll.FileManager;
import com.mlb.ytbsubtitles.bll.StorageFileNotFoundException;
import com.mlb.ytbsubtitles.bll.SubtitleManager;
import com.mlb.ytbsubtitles.bo.File;
import com.mlb.ytbsubtitles.bo.Subtitle;

@Controller
@SessionScope
public class SubtitleController {
	
	@Autowired
	private FileManager fileManager;
	@Autowired
	private SubtitleManager subtitleManager;
	
	/**
	 * Call when user clic on the add button for new subtitle
	 * the button is in the fileSelected.html file
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/NewSubtitle/{id}")
	public String newSubtitle(@PathVariable String id, Model model) {

		File file = fileManager.getFile(id);
		model.addAttribute("file",file);

		Subtitle subtitle = new Subtitle();
		// possible with bidirectional to remove {id} from Post request
		// see method addSubtitle below
		// subtitle.setFile(file);
		model.addAttribute("subtitle", subtitle);

		return "newSubtitle";
	}

	/**
	 * Call when user validate the form to add a new subtitle
	 * @param subtitle
	 * @param bindingResult
	 * @param model
	 * @param id could be omit with bidirectional association
	 * between Subtitle and File Objects. Keep it for example.
	 * @return
	 */
	@PostMapping("/NewSubtitle/{id}")
	public String addSubtitle(@Valid Subtitle subtitle,
			BindingResult bindingResult,
			Model model,
			@PathVariable String id) {

		File file = fileManager.getFile(id);
		//
		if (bindingResult.hasErrors()) {
			model.addAttribute("file",file);
			return "newSubtitle";
		}

		// valid subtitle, adding it to file
		file.addSubtitle(subtitle);
		fileManager.addFile(file);
		//System.out.println("id modif = " + fileId);

		model.addAttribute("file",file);
		return "fileSelected";	
	}

	/**
	 * Method called when user want to delete a Subtitle
	 * 
	 * @param model
	 * @param id is the id of the file
	 * @param index is the index on user screen,
	 *  it is different from subtitle's id in DB
	 * @return page fileSelected.html with existing error
	 */
	@PostMapping(value="DeleteSubtitle/{id}")
	public String deleteSubtitle(Model model,
			@PathVariable String id,
			@RequestParam("index") String index) {
		String errorDelete="";
		errorDelete = fileManager.deleteSubtitleFromFile(id, index);

		model.addAttribute("file", fileManager.getFile(id));
		if (!errorDelete.isEmpty()) {
			model.addAttribute("errorDelete", errorDelete);
		}

		return "fileSelected";
	}
	
	/**
	 * Method call when user want to edit a subtitle
	 * @param model
	 * @param id of the file in list on user's screen
	 * @param index of the subtitle in list on user's screen
	 * @return form page editSubtitle if index found
	 * or fileSelected
	 */
	@PostMapping(value="EditSubtitle/{id}")
	public String editSubtitle(Model model,
			@PathVariable String id,
			@RequestParam("index") String index){
		Subtitle subtitle;
		String errorEdit;
		
		try {
		// keep in mind index is the user's screen index
		subtitle = subtitleManager.getSubtitleByIndexForFile(id, index);
		
		model.addAttribute("fileId", subtitle.getFile().getId());
		model.addAttribute(subtitle);
		return "editSubtitle";
		
		} catch (NumberFormatException|StorageFileNotFoundException e) {
			errorEdit = e.getMessage();
			model.addAttribute("errorEdit",errorEdit);
			File file = fileManager.getFile(id);
			model.addAttribute("file",file);
			return "fileSelected";
		}
	}
	
	
	@PostMapping(value="SaveChangedSubtitle/{fileId}")
	public String saveChangedSubtitle(Model model,
			@PathVariable String fileId,
			@Valid Subtitle subtitle,
			BindingResult bindingResult) {
		File file;
		//
		if (bindingResult.hasErrors()) {
			model.addAttribute("fileId",fileId);
			model.addAttribute("subtitle",subtitle);
			return "editSubtitle";
		}
		// valid subtitle, setting File
		file = fileManager.getFile(fileId);
		subtitle.setFile(file);
		// then update database
		subtitleManager.addSubtitle(subtitle);

		model.addAttribute("okMessage","OK");
		model.addAttribute("fileId",fileId);
		model.addAttribute(subtitle);
		System.out.println(subtitle);

		return "editSubtitle";
	}
}
