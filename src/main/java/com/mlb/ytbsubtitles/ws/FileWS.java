package com.mlb.ytbsubtitles.ws;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mlb.ytbsubtitles.bll.FileManager;
import com.mlb.ytbsubtitles.bo.File;
import com.mlb.ytbsubtitles.dal.FileRepository;

@RestController
@RequestMapping("/API/file")
public class FileWS {
	
	@Autowired
	private FileRepository fileRepository;
	
	/**
	 * Method is call to create new empty File
	 * use /API/file/newFile
	 */
	@GetMapping("/newFile")
	public File getNewFile() {
		return new File("");
	}

	/**
	 * Method to get all the files from database
	 * with /API/file/all
	 * Note that the Subtitle bidirectional association with
	 * the file_id is not return otherwise there is a infinite loop
	 * @return json
	 */
	@GetMapping(value="/all")
	public List<File> WSgetAllFiles(){
		return fileRepository.findAll();
		
	}
	
	/**
	 * Method call to add a File with filename
	 * @param file
	 * @return HTTP code 201 or XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	 */
	@PostMapping(value="/saveFile")
	public ResponseEntity<?> addFile(@RequestBody File file){
		
		File fileAdded = fileRepository.save(file);
		
		if (fileAdded == null)
			return ResponseEntity.noContent().build();
		
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(fileAdded.getId())
				.toUri();

		return ResponseEntity.created(location).build();
	}
}
