package com.mlb.ytbsubtitles.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mlb.ytbsubtitles.bll.SubtitleManager;
import com.mlb.ytbsubtitles.bo.File;
import com.mlb.ytbsubtitles.bo.Subtitle;
import com.mlb.ytbsubtitles.dal.SubtitleRepository;

@RestController
@RequestMapping("/API/subtitle")
public class SubtitleWS {
	
	@Autowired
	SubtitleRepository subtitleRepository;
	
	/**
	 * Method is call to create new empty Subtitle
	 * use /API/subtitle/newSubtitle
	 */
	@GetMapping("/newSubtitle")
	public Subtitle getNewSubtitle() {
		return new Subtitle();
	}

}
