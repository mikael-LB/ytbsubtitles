package com.mlb.ytbsubtitles;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.mlb.ytbsubtitles.bo.File;
import com.mlb.ytbsubtitles.bo.Subtitle;
import com.mlb.ytbsubtitles.dal.FileRepository;
import com.mlb.ytbsubtitles.dal.SubtitleRepository;

@SpringBootTest
class YtbsubtitlesApplicationTests {
	
	

	@Test
	void contextLoads() {
	}
	
	/**
	 * Test adding a Subtitle
	 */
	@Autowired
	private SubtitleRepository dao;
	private Subtitle subtitle1 = new Subtitle();

	@Test
	void saveSubtitle() {
		dao.save(subtitle1);
	}
	
	/**
	 * Test adding a File
	 */
	@Autowired
	private FileRepository daoFile;
	
	@Test
	void saveFile() {
		File file = new File("filename");
		daoFile.save(file);
	}

}
